<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BannerController extends Controller {

    /**
     * @Route("/")
     * 
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $banners = $em->getRepository('AppBundle:Banner')->findActiveAtSpot(1);
        if (!empty($banners)) {
            $rand_keys = array_rand($banners, 1);
            $spot1 = $banners[$rand_keys];
        } else {
            $spot1 = NULL;
        }

        $banners = $em->getRepository('AppBundle:Banner')->findActiveAtSpot(2);
        if (!empty($banners)) {
            $rand_keys = array_rand($banners, 1);
            $spot2 = $banners[$rand_keys];
        } else {
            $spot2 = NULL;
        }


        return $this->render('AppBundle:Banner:index.html.twig', array(
                    'spot1' => $spot1,
                    'spot2' => $spot2,
        ));
    }

}
