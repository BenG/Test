<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BannerAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('spot', 'choice', array('choices' => array(
                        '1' => 'Banner1',
                        '2' => 'Banner2',
            )))
                ->add('start', NULL)
                ->add('expire', NULL)
                ->add('file', 'file', array('required' => false));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('expire', NULL);
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('spot')
                ->add('start')
                ->add('expire');
    }

    public function prePersist($image) {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image) {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image) {
        if ($image->getFile()) {
            $image->refreshUpdated();
        }
    }

}
